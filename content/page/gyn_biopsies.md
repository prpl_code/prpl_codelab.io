----
Title: GYN Biopsies
slug: "gyn_biopsies"
----


| **Zuber Nakadar, MD., MBA** |            |   | **Harshwardhan Thaker, MD., PhD.** |            |
|-----------------------------|------------|---|------------------------------------|------------|
| Chief Lab Supervisor        | 01/02/2014 |   | Medical Director                   | 01/02/2014 |
|                             |            |   |                                    |            |
| Reviewed by                 |            |   | Approved by                        |            |

**PURPOSE:** 
=============

-   To set guidelines on how to grossly describe GYN biopsies as to: size,
    shape, color, texture, number of pieces, and measurement.

-   Order additional tests based on the diagnosis.

-   Enter the number of cassettes and levels needed.

**POLICY**:

-   Gross one specimen at a time. Meaning: There should only be **one** bottle
    on the grossing table at a time.

-   Match the bottle with the requisition form and the computer generated label
    as to:

    -   Patient’s name

    -   Site (s)

-   Gross only specimens fixed in 10% neutral buffered formalin.

-   If received in other fixatives, or no fixative, or fresh, bring it to the
    attention of the supervisor.

-   If there is no fixative, do not leave at room temperature. Refrigerate if
    received at night. Immediately bring it to the supervisor’s attention.

-   If the prefix is not correct, do not gross. Verify with the supervisor.

-   If there is error of any kind, do not gross. Bring it to the attention of
    the supervisor.

-   If it is deemed complicated or not sure, send to the resident or surgical
    fellow to gross.

-   Specimens going to Resident / PA and vice-versa must be documented on the
    log book provided.

-   “No visible tissue” seen must be verified by pathologist and follow their
    instruction to filter them or submit for cytospin / cell block preparation.

-   Add 10% neutral buffered formalin to the left over fresh tissue in the
    specimen container.

-   Wash forceps in between two specimen containers to avoid floaters. Change
    cleaning solution as often as possible.

-   Provide separate containers according to prefixes to hold the specimen
    containers temporarily after grossing.

-   If a biopsy specimen bottle contains no tissues and it does not belong to
    Cytology.

-   Send it to pathologist to confirm, sign, and date the requisition form.

    -   If the specimen is clear, let cytology technician prepares a cytospin

    -   If the specimen is a GYN specimen, let cytology technician centrifuge
        and submit a cell block

    -   Give the cytospin slide with a copy of the requisition form to the
        histology supervisor.

-   Use a Statmark pen to write on the side of the cassette the following:

    -   “Punch” for a pinch biopsy

    -   “ Oral” for an oral specimen

    -   “ Hirsch” for Hirsch sprung”

    -   “Tick”

    -   Number of pieces of tissue:

        -   Tissue fragments, count the number of tissue fragments.

            -   Except POC’s, EMC, EMB, mucus.

        -   Cores, count the number of cylindrical structures

        -   Write the number of pieces **after** the tissue has been bisected,
            trisected, or serially sectioned.

-   For all biopsies, **submit all tissue fragments.**

**MATERIALS:**
==============

-   Lens paper and linings

-   Plastic organizer and containers

-   Forceps

-   Marking dyes

-   10% neutral buffered formalin

-   Applicator sticks and plastic pipette

-   Scissor

-   Paper towel and formalin absorbent pads

-   Scalpel , blade and holder

-   Pencils, Statmark, markers, and pens

-   Acetic acid-alcohol, 8.6 %

-   Downdraft hoods

-   Decal Stat

-   Bleach solution, 10:1

**PREPARATION OF CHEMICALS**
============================

-   **Acetic acid-alcohol, 8.6%**

    -   Acetic acid, glacial…………………..8.6.ml

    -   Dehydrant, 95% alcohol……………91.4 ml.

    -   Shelf life: One year

    -   Storage: Room temperature

-   **Decal Stat**

    -   Commercially prepared: Decal Corporation

    -   Storage: Room temperature, Acid cabinet

    -   Shelf life: See manufacturer’s label

-   **Neutral Buffered Formalin, 10%**

    -   Commercially prepared by Richard Allan Scientific

    -   Storage: Room temperature, Flammable cabinet

    -   Shelf life: See manufacturer’s label

-   **Bleach solution, 10:1**

    -   Bleach stock solution……………… 1 part

    -   Water……………………………… 9 parts

    -   Shelf life: One week

    -   Storage: Room temperature

**PROCEDURE:**

-   Arrange the specimen bottles and the request forms in numerical order.

-   Scan Specimen jar barcode in PowerPath and print cassette using Leica
    cassette printer.

-   Predefined cassette will be printed as per specimen type.

Prefix for Surgical

Year

PS13-22238 A1 Block part and number

Case number

Bottom part of the cassette

Lid of the cassette

-   Match all the information on the specimen bottle with the request form.

-   Get the specimen bottle and the corresponding cassette

-   Use forceps to remove specimen from the specimen bottle

-   Put the specimen on a foam biopsy pad on the bottom part of the cassette.

-   **Do not crush** the specimens.

-   Secure the specimen by putting another foam biopsy pad on top of it.

-   **Illustrations:**

3 pieces of tissue

>   Use this second foam to cover the tissue fragments.

**PROCEDURE FOR Bx2, less than 0.1 to 1mm with mucus, EMB:**

-   If the tissue is mucoid, an EMB, POC, or even friable tissue, filter, and
    wrap in lens paper.

-   Apply the “4-fold method of wrapping”.

-   See SOP G2 – Grossing Manual for details

**PROCEDURE FOR PRODUCT OF CONCEPTION FIXED IN 10% NEUTRAL BUFFERED FORMALIN:**

-   Remove the specimen from the specimen bottle.

-   Put on a clean paper towel.

-   Tease the tissue fragments to look for chorionic villi (spongy material).

-   Submit from 1-5 cassettes depending on the amount of the material received.

-   Wrap in lens paper following method, See SOP G2 – Grossing Manual for
    details.

**TAKING MEASUREMENTS**

-   L x H x W in centimeters

    -   Put the specimen at the center of the

    -   Lens paper then take measurements.

-   Wrap in lens paper following method.

**REFERENCES**:

-   Mount Sinai School of Medicine, Standard Operating Procedures

-   Dr. Tamara Kalir, GYN Pathologist, 11/2013

**ORIGINAL DATE / HISTORY**:

-   Original date of this procedure was January 7th 1983.

-   This procedure has been updated and modified by Zuber Nakadar, Chief Lab
    Supervisor on 11/07/2013 to reflect the current header format.
