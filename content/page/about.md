---
title: About us
subtitle: Why you'd want to hang out with us
comments: false
---

We are pathology residents at the Icahn School of Medicine at Mount Sinai collaboratively developing software solutions

Projects include:

*  A grossing wiki
*  Automated adaptive gross dictation software
*  Data extraction software to parse narrative pathology reports into analysis ready data

### Leadership
**Aryeh Stock MD** is currently a PGY-2 resident with an interest in medical informatics and software development



